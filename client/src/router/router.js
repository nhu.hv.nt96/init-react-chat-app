import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import JoinComponent from '../components/join/join'
import ChatComponent from '../components/chat/chat'

const RouterService = () => (
  <Router>
    <Route path="/" exact component={JoinComponent}/>
    <Route path="/chat" exact component={ChatComponent}/>
  </Router>
)

export default RouterService