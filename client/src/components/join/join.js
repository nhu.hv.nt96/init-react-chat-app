import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import './join.css'

const JoinComponent = () => {
  const [name, setName] = useState('')
  const [room, setRoom] = useState('')

  const handleOnchangeName = (event) => {
    const { name, value } = event.target
    setName(value)
  }

  const handleChangeRoom = (event) => {
    const { name, value } = event.target
    setRoom(value)
  }

  const hanldeOnClick = (event) => {
    if (!name || !room) event.preventDefault()
  }

  return (
    <div className="joinOuterContainer">
      <div className="joinInnerContainer">
        <h1 className="heading">Join</h1>
        <div><input placeholder="Name" className="joinInput" type="text" onChange={handleOnchangeName} /></div>
        <div><input placeholder="Room" className="joinInput mt-20" type="text" onChange={handleChangeRoom} /></div>
        <Link onClick={hanldeOnClick} to={`/chat?name=${name}&room=${room}`} >
          <button className='button mt-20' type="submit">Sign In</button>
        </Link>
      </div>
    </div>
  )
}

export default JoinComponent
