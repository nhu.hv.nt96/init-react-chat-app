import React from 'react';
import ReactDOM from 'react-dom';
// import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/scss/bootstrap.scss'
import './assets/css/styles.scss';
// import store from './app/app.store'
// import AppRouter from './app/app.router'
import Router from './router/router'
// import { Provider } from 'react-redux'

ReactDOM.render(
  <Router />,
  document.getElementById('root'));