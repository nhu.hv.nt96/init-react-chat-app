// import PopupModal from '../../../../services/models/base/popup/popup.modal'
const stateDefault = {}

const popup = (state = stateDefault, action) => {
  switch (action.type) {
    case 'POPUP_UPDATE':
      return action.payload
    default:
      return state
  }
}

export default popup