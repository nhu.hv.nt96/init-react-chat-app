const stateDefault = {}
const login = (state = stateDefault, action) => {
    switch (action.type) {
        case 'MESSAGE_CODE_RESPONSE_UPDATE':
            return action.payload
        default:
            return state
    }
}

export default login