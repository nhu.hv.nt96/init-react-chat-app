const stateDefault = {
  username: '',
  password: ''
}
const login = (state = stateDefault, action) => {
  switch (action.type) {
    case 'USERNAME':
      return { ...state, username: action.payload }
    case 'PASSWORD':
      return { ...state, password: action.payload }
    case 'LoginAction': {
      return {
        username: action.payload.username,
        password: action.payload.password
      }
    }
    default:
      return state
  }
}

export default login