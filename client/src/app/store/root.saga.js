import { all } from 'redux-saga/effects'
import WatcherSaga from './saga/watcher.saga'

export default function* rootSaga() {
  yield all([
    WatcherSaga.login()
  ])
}