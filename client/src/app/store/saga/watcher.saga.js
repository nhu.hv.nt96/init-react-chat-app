import { takeLatest } from 'redux-saga/effects'
import LoginSaga from './public/login.saga'

class WatcherSaga {

  *login() {
    yield takeLatest("LoginAction", LoginSaga.login.bind(LoginSaga))
  }
}

export default new WatcherSaga()