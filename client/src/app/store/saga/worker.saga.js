import { put } from 'redux-saga/effects'
import MessageCode from '../../services/codeResponse/message/message.code'
import LoogerService from '../../services/logger/logger.services'
class WorkerSaga {
  constructor() {
    this.code = new Map([])
    this.code.set(MessageCode.LOGIN_SUCCESS, this.putAuthUpdate.bind(this))
  }

  * save(response) {
    LoogerService.info("WorkerSaga excute save")
    LoogerService.debug("receipt response", response)
    try {
      if(this.code.has(response.code)) {
        yield this.code.get(response.code)(response.data)
      }
      yield this.putMessageCodeResponseUpdate(response)
    } catch (e) {
      LoogerService.error(`WorkerSaga save ${e.toString()}`)
    }

  }

  * putMessageCodeResponseUpdate(payload) {
    LoogerService.info("WorkerSaga excute putMessageCodeResponseUpdate")
    LoogerService.debug("receipt payload", payload)
    try {
      yield put({
        type: 'MESSAGE_CODE_RESPONSE_UPDATE',
        payload: payload
      })
    } catch (e) {
      LoogerService.error(`WorkerSaga putMessageCodeResponseUpdate ${e.toString()}`)
    }
  }

  * putAuthUpdate(payload) {
    LoogerService.info("WorkerSaga excute putAuthUpdate")
    LoogerService.debug("receipt payload", payload)
    try {

    } catch(e) {
      LoogerService.error(`WorkerSaga putAuthUpdate ${e.toString()}`)
    }
  }
}

export default new WorkerSaga()