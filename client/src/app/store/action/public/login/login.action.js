import LoggerService from '../../../../services/logger/logger.services'
class LoginAction {
  constructor() {

  }

  login(data) {
    LoggerService.info("LoginAction excute login")
    LoggerService.debug("payload", data)
    try {
      return {
        type: "LoginAction",
        payload: data
      }
    } catch (e) {
      LoggerService.error(`LoginAction login ${e.toString()}`)
    }
  }

}

export default new LoginAction()