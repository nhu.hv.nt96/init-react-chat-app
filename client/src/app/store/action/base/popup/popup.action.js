import LoggerService from '../../../../services/logger/logger.services'
class PopupAction {
  constructor() {

  }

  popup(data) {
    LoggerService.info("PopupAction excute login")
    LoggerService.debug("payload", data)
    try {
      return {
        type: "POPUP_UPDATE",
        payload: data
      }
    } catch (e) {
      LoggerService.error(`PopupAction login ${e.toString()}`)
    }
  }

}

export default new PopupAction()