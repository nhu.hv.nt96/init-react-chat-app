import { combineReducers } from 'redux'
import login from './reducer/public/login/login'
import popup from './reducer/base/popup/popup'
import messageCodeResponse from './reducer/messageCodeResponse/message.code.response'

export default combineReducers({
    login,
    messageCodeResponse,
    popup
  })