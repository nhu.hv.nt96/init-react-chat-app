import React, { Component } from 'react'
import PublicRouter from './ux/public/public.router'
import PublicContainer from './ux/public/public.container'
import UserRouter from './ux/user/user.router'
import UserContainer from './ux/user/user.container'
import {
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";


class AppRouter extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Router>
        <Switch>
          <PublicRouter path="/public">
            <PublicContainer />
          </PublicRouter>
          <UserRouter path="/user">
            <UserContainer />
          </UserRouter>
          <Redirect to="/public" />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter