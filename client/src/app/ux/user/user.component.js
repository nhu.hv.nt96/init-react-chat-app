import React, { Component, Fragment } from 'react'
import LoggerService from '../../services/logger/logger.services'
import AuthService from '../../services/auth/auth.service'

export default class UserComponent extends Component {
  constructor(props) {
    super(props)
    this.handleLogout = this.handleLogout.bind(this)
  }

  handleLogout() {
    LoggerService.info("UserComponent excute handleLogout")
    try {
      const { history } = this.props
      AuthService.logout()
      history.push('/public')
    } catch (e) {
      LoggerService.error(`UserComponent handleLogout ${e.toString()}`)
    }
  }

  render() {
    return (
      <Fragment>
        <p style={{ textAlign: "center" }}>Đầy là UserComponent</p>
        <input type="submit" value="Logout" onClick={this.handleLogout} />
      </Fragment>

    )
  }
}