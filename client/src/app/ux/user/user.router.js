import React, { Component } from "react";
import AuthService from '../../services/auth/auth.service'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";

const auth = true

function UserRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
      AuthService.getAuth() === "USER" ? (
          children
        ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
      }
    />
  );
}

export default UserRoute