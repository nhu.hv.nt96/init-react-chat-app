import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import ConfirmComponent from './confirm.component'

const mapPropstoState = ({popup}) => {
  return {
    popup
  }
}

const mapDispatchToProps = () => {
  return {

  }
}

export default withRouter(connect(mapPropstoState, mapDispatchToProps)(ConfirmComponent))