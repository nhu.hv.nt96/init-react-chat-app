import React, { Component } from 'react'
import CofirmHtml from '../../../../ui/base/popup/confirm/confirm.html'

class ConfirmComponent extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { handleClose,handleConfirm ,popup } = this.props
    return (
      <CofirmHtml
      popup={popup}
      handleClose={handleClose}
      handleConfirm={handleConfirm}
      />
    )
  }


}

export default ConfirmComponent