import React, { Component } from 'react'
import NotiHtml from '../../../../ui/base/popup/noti/noti.html'

class NotiComponent extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { handleClose, handleConfirm ,popup } = this.props
    return (
      <NotiHtml
      popup={popup}
      handleClose={handleClose}
      handleConfirm={handleConfirm}
      />
    )
  }


}

export default NotiComponent