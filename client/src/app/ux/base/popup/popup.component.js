import React, { Component } from 'react'
import PopupHtml from '../../../ui/base/popup/popup.html'
import PopupLayout from '../../../services/enum/layout/popup.layout'
import NotiContainer from './noti/noti.container'
import ConfirmContainer from './confirm/confirm.container'
import LoggerService from '../../../services/logger/logger.services'

class PopupComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      view: {
        current: PopupLayout.default,
        noti: PopupLayout.noti,
        confirm: PopupLayout.confirm
      }
    }
    this.handleClose = this.handleClose.bind(this)
    this.handleConfirm = this.handleConfirm.bind(this)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    LoggerService.info("PopupComponent excute UNSAFE_componentWillReceiveProps")
    LoggerService.debug("nextProps", nextProps)
    try {
      const { view } = this.state
      if(this.props.popup !== nextProps.popup) {
        view.current = nextProps.popup.view
        this.setState({
          view: view
        })
      }
    } catch (e) {
      LoggerService.error(`PopupComponent UNSAFE_componentWillReceiveProps ${e.toString()}`)
    }
  }

  handleClose() {
    LoggerService.info("PopupComponent excute handleClose")
    try {
      // event.preventDefault()
      const { view } = this.state
      view.current = PopupLayout.default
      this.setState({
        view: view
      })
    } catch (e) {
      LoggerService.error(`PopupComponent handleClose ${e.toString()}`)
    }
  }

  async handleConfirm() {
    LoggerService.info("PopupComponent excute handleConfirm")
    try {
      const { view } = this.state
      const { popup } = this.props
      await popup.func.handleConfirm()
      await this.handleClose()
    } catch (e) {
      LoggerService.error(`PopupComponent handleConfirm ${e.toString()}`)
    }
  }

  render() {
    const { view } = this.state
    console.log("view", view)
    return (
      <PopupHtml
        view={view}
        handleClose={this.handleClose}
        handleConfirm={this.handleConfirm}
        NotiContainer={NotiContainer}
        ConfirmContainer={ConfirmContainer}
        
      />
    )
  }
}

export default PopupComponent