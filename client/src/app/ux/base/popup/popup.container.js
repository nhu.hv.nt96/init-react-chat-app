import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import PopupComponent from './popup.component'

const mapStateToProps = ({ popup }) => {
  return {
    popup
  }
};

const mapDispatchToProps = (dispatch) => {
  return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PopupComponent))