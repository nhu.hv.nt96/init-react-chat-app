import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import ForgotComponent from './forgot.component'

const mapStateToProps = ({ }) => {
    return {}
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ForgotComponent))