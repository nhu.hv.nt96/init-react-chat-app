import React, { Component } from "react";
import AuthService from '../../services/auth/auth.service'
import {
  Route,
  Redirect,
} from "react-router-dom";

const auth = true

function PublicRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        AuthService.getAuth() === "PUBLIC" ? (
          children
        ) : (
            <Redirect
              to={{
                pathname: "/user",
                state: { from: location }
              }}
            />
          )
      }
    />
  );
}

export default PublicRoute