import React, { Component } from 'react'
import LoginHtml from '../../../ui/public/login/login.html'
import Data from '../../../services/models/public/login.model'
import ProcessCodeResponse from '../../../services/codeResponse/process/process'
import LoggerService from '../../../services/logger/logger.services'
import AuthService from '../../../services/auth/auth.service'
import CacheDataService from '../../../services/cache/cache.data'
import PopupLayout from '../../../services/enum/layout/popup.layout'
import PopupModal from '../../../services/models/base/popup/popup.modal'
import CacheLayoutService from '../../../services/cache/cache.layout'
import HelperService from '../../../services/helper/helper.service'
import Localize from '../../../services/localize/localize.service'

export default class LoginComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      timeout: {
        field: '',
        message: '',
        status: false,
        timeout: null
      },
      data: new Data(),
      ui: {
        ishowBtn: true,
      },
      func: {
        handleLoginSuccess: this.handleLoginSuccess.bind(this)
      }
    }
    this.handleOnChange = this.handleOnChange.bind(this)
    this.handleTimeout = this.handleTimeout.bind(this)
    this.handleResult = this.handleResult.bind(this)
    this.handleRequest = this.handleRequest.bind(this)
    this.handlePopup = this.handlePopup.bind(this)
    this.handleRedirect = this.handleRedirect.bind(this)
    this.handleChangeLanguage = this.handleChangeLanguage.bind(this)
  }

  handleLoginSuccess() {
    LoggerService.log("LoginComponent excute  handleLoginSuccess")
    try {
      const { history } = this.props
      AuthService.setAuth('USER')
      CacheDataService.set('USER')
      history.push('/user')
    } catch (e) {
      LoggerService.error(`LoginComponent handleLoginSuccess ${e.toString()}`)
    }
  }

  UNSAFE_componentWillMount() {
    LoggerService.info("LoginComponent excute  UNSAFE_componentWillMount")
    try {
      // const { match } = this.props
      // const cache = CacheLayoutService.get(HelperService.hashMD5("Ada"))
      // console.log("cache", cache)
    } catch (e) {
      LoggerService.error(`LoginComponent UNSAFE_componentWillMount ${e.toString()}`)
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    LoggerService.info("LoginComponent excute  UNSAFE_componentWillReceiveProps")
    LoggerService.debug("nextProps", nextProps)
    try {
      if (this.props.messageCodeResponse !== nextProps.messageCodeResponse) {
        ProcessCodeResponse.checkCode(nextProps.messageCodeResponse.code, this.state)
      }
      this.handleResult()
    } catch (e) {
      LoggerService.error(`LoginComponent UNSAFE_componentWillReceiveProps ${e.toString()}`)
    }
  }

  handleOnChange(event) {
    LoggerService.info("LoginComponent excute handleOnChange")
    LoggerService.debug("name", event.target.name)
    LoggerService.debug("value", event.target.value)
    try {
      const { match } = this.props
      const { name, value } = event.target
      const { data, ui } = this.state
      data[name] = value
      const ishowBtn = Object.values(data.getValueLogin()).indexOf('') === -1
      if (ishowBtn) {
        CacheLayoutService.set(match.url, "LeeSin", data.getValueLogin())
        ui.ishowBtn = false
        this.setState({
          ui: ui
        })
      }
    } catch (e) {
      LoggerService.error(`LoginComponent handleOnChange ${e.toString()}`)
    }
  }

  handleTimeout(event) {
    LoggerService.info("LoginComponent excute handleTimeout")
    try {
      event.preventDefault()
      const { timeout } = this.state
      timeout.status = true
      this.setState({
        timeout: timeout
      })
      this.handleRequest()
      timeout.timeout = setTimeout(() => {
        timeout.field = "all"
        timeout.message = "Hệ thống bận. Vui lòng thử lại sau"
        timeout.status = false
        this.setState({
          timeout: timeout
        })
      }, 5000);
    } catch (e) {
      LoggerService.error(`LoginComponent handleTimeout ${e.toString()}`)
    }
  }

  handleResult(field = '', message = '') {
    LoggerService.info("LoginComponent excute handleResult")
    try {
      const { timeout } = this.state
      timeout.field = field
      timeout.message = message
      timeout.status = false
      clearTimeout(timeout.timeout)
      this.setState({
        timeout: timeout
      })
    } catch (e) {
      LoggerService.error(`LoginComponent handleResult ${e.toString()}`)
    }
  }

  handleRequest() {
    LoggerService.info("LoginComponent excute handleRequest")
    try {
      const { loginAction } = this.props
      const { data } = this.state
      loginAction(data.getValueLogin())
    } catch (e) {
      LoggerService.error(`LoginComponent handleRequest ${e.toString()}`)
    }
  }

  handlePopup() {
    LoggerService.info("LoginComponent excute handlePopup")
    try {
      const { popupAction } = this.props
      const popupModal = new PopupModal()

      popupModal.setTitle("Đăng xuất")
      popupModal.setContent("Bạn có muốn đăng xuất")
      popupModal.setHandleConfirm(() => { console.log("aca") })
      popupAction(popupModal)
    } catch (e) {
      LoggerService.error(`LoginComponent handlePopup ${e.toString()}`)
    }
  }

  handleRedirect(url) {
    LoggerService.info("LoginComponent excute handleRedirect")
    LoggerService.debug("url", url)
    try {
      const { history, match, popupAction } = this.props
      const cache = CacheLayoutService.get(HelperService.hashMD5(match.url))
      if (cache) {
        const popupModal = new PopupModal()
        popupModal.setView(PopupLayout.noti)
        popupModal.setTitle("Đăng xuất")
        popupModal.setContent("Bạn có muốn đăng xuất")
        popupModal.setHandleConfirm(() => {
          CacheLayoutService.clear()
          history.push(url)
        })
        popupAction(popupModal)
      } else {
        history.push(url)
      }

    } catch (e) {
      LoggerService.error(`LoginComponent handleRedirect ${e.toString()}`)
    }
  }

  handleChangeLanguage(language) {
    LoggerService.info("LoginComponent excute handleChangeLanguage")
    console.log("language", language)
    this.setState({
    })
    try {
      Localize.changeLanguage(language)
    } catch (e) {
      LoggerService.error(`LoginComponent handleChangeLanguage ${e.toString()}`)
    }
  }



  render() {
    const { ui, timeout } = this.state
    console.log("Localize", Localize.getCache())
    return (
      <LoginHtml
        ui={ui}
        timeout={timeout}
        handleOnChange={this.handleOnChange}
        handleTimeout={this.handleTimeout}
        handleResult={this.handleResult}
        handlePopup={this.handlePopup}
        handleRedirect={this.handleRedirect}
        handleChangeLanguage={this.handleChangeLanguage}
      />
    )
  }
}