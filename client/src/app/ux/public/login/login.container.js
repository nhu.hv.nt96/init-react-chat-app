import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import LoginComponent from './login.component'
import LoginAction from '../../../store/action/public/login/login.action'
import PopupAction from '../../../store/action/base/popup/popup.action'
const mapStateToProps = ({login, messageCodeResponse}) => {
  return {
    login,
    messageCodeResponse
  }
};

const mapDispatchToProps = (dispatch) => {

  return {
    loginAction: (data) => {
      dispatch(LoginAction.login(data))
    },
    popupAction: (data) => {
      dispatch(PopupAction.popup(data))
    }
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginComponent))