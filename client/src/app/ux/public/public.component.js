import React, { Component, Fragment } from 'react'
import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import LoginContainer from './login/login.container'
import ForgotContainer from './forgot/forgot.container'
import RegisterContainer from './register/register.container'
import PopupContainer from '../base/popup/popup.container'

export default class PublicComponent extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { match } = this.props
    return (
      <Fragment>
        <Switch>
          <Route exact path={`${match.url}/login`} component={LoginContainer} />
          <Route exact path={`${match.url}/forgot`} component={ForgotContainer} />
          <Route exact path={`${match.url}/register`} component={RegisterContainer} />
          <Redirect to={`${match.url}/login`} />
        </Switch>
        <PopupContainer/>
      </Fragment>
    )
  }
}