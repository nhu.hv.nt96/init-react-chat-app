import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import PublicComponent from './public.component'

const mapStateToProps = ({ }) => {
    return {}
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PublicComponent))