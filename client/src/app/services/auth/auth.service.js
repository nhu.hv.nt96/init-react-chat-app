import CacheDataService from '../cache/cache.data'
import LoggerService from '../logger/logger.services'
class AuthService {

  constructor() {
    if (!AuthService.instance) {
      this.auth = "PUBLIC"
      AuthService.instance = this
    }
    return AuthService.instance
  }

  getAuth() {
    LoggerService.info("AuthService excute getAuth")
    try {
      const auth = CacheDataService.get()
      this.auth = auth.auth
      return this.auth
    } catch (e) {
      // LoggerService.error(e.toString())
      return this.auth
    }

  }

  setAuth(auth) {
    this.auth = auth
  }

  logout() {
    this.setAuth('PUBLIC')
    window.sessionStorage.clear();
  }
}

export default new AuthService()
