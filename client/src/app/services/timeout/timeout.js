class TimeoutServices {
  constructor(setState) {
    // if (!TimeoutServices.instance) {
      this.field = ''
      this.message = ''
      this.timeout = null
      this.setState = setState
      this.status = false
      this.setTimeout = this.setTimeout.bind(this)
      TimeoutServices.instance = this
    // }
    // return TimeoutServices.instance
  }

  setTimeout(status = true, field = '', message = '') {
    try {
      this.setState({
        timeout: {
          field: field,
          message: message,
          status: status
        }
      })
  
      clearTimeout(this.timeout)
     
      if (status) {
        this.timeout = setTimeout(() => {
          // this.setTimeout(false, "all", "Hệ thống bận. Vui lòng thử lại sau")
        }, 2000);
      }
    } catch (e) {
    }
  }
}

export default  TimeoutServices