import LoggerService from '../logger/logger.services'
import HelperService from '../helper/helper.service'

class CacheLayoutService {
  constructor() {
  }
  set(url, view, data) {
    LoggerService.info("CacheLayoutService excute set")
    LoggerService.debug("url", url)
    LoggerService.debug("view", view)
    LoggerService.debug("data", data)
    try {
      const key = HelperService.hashMD5(url)
      let value = this.get(key)
      if(!value) {
        value = {}
      }
      value[view] = data
      value = JSON.stringify(value)
      window.sessionStorage.setItem(key, value)
    } catch (e) {
      LoggerService.error(`CacheLayoutService set ${e.toString()}`)
    }
  }

  get(key) {
    LoggerService.info("CacheLayoutService excute get")
    LoggerService.debug("key", key)
    try {
      let value = window.sessionStorage.getItem(key)
      if (value) {
        value = JSON.parse(value)
        return value
      }
      value = null
      return value
    } catch (e) {
      LoggerService.error(`CacheLayoutService get ${e.toString()}`)
    }
  }

  getView(url, view) {
    LoggerService.info("CacheLayoutService excute getView")
    LoggerService.debug("url", url)
    LoggerService.debug("view", view)
    try {
      const key = HelperService.hashMD5(url)
      let value = this.get(key)
      if(!value) {
        return null
      }
      value = value[view]
      if(!value) {
        return null
      }
      return value
    } catch (e) {
      LoggerService.error(`CacheLayoutService getView ${e.toString()}`)
    }
  }

  clear() {
    LoggerService.info("CacheLayoutService excute clear")
    try {
      window.sessionStorage.clear()
    } catch (e) {
      LoggerService.error(`CacheLayoutService clear ${e.toString()}`)
    }
  }

}

export default new CacheLayoutService()