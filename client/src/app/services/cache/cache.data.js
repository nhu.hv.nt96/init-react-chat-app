import LoogerService from '../logger/logger.services'
import HelperService from '../helper/helper.service'
import AppConf from '../../../config/app.conf.json'
class CacheDataService {
  constructor() {

  }

  set(auth) {
    LoogerService.info("CacheDataService excute set")
    try {
      const key = HelperService.hashMD5(AppConf.key)
      let value = {
        key: HelperService.encryptAES(HelperService.hashMD5(key), HelperService.hashMD5(key)),
        auth: auth
      }
      value = JSON.stringify(value)
      value = value = HelperService.encryptAES(value, HelperService.hashMD5(key))
      window.sessionStorage.setItem(key, value)
    } catch (e) {
      LoogerService.error(`CacheDataService set ${e.toString()}`)
    }

  }

  get() {
    LoogerService.info("CacheDataService excute get")
    try {
      const key = HelperService.hashMD5(AppConf.key)
      let value = window.sessionStorage.getItem(key)
      if(!value) {
        throw new Error(`value is null`)
      }
      value = HelperService.decryptAES(value, HelperService.hashMD5(key))
      value = JSON.parse(value)
      if (HelperService.decryptAES(value.key, HelperService.hashMD5(key)) !== HelperService.hashMD5(key)) {
        throw new Error('session key error')
      }
      return value
    } catch (e) {
      throw new Error(e.toString())
    }
  }

}

export default new CacheDataService()