export default class LoginModel {
  constructor() {
    this.username = ''
    this.password = ''
  }

  getValueUsername() {
    return {
      username: this.username
    }
  }

  getValuePassword() {
    return {
      password: this.password
    }
  }

  getValueLogin() {
    return {
      username: this.username,
      password: this.password
    }
  }
}
