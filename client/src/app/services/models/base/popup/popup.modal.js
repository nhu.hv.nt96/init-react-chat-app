class PopupModal {
  constructor() {
      this.time = new Date().getTime()
      this.view = ''
      this.title = ''
      this.content = ''
      this.func = {
        handleConfirm: () => {}
      }
  }

  setView(view) {
    this.view = view
  }

  getView() {
    return this.view
  }

  setTitle(title) {
    this.title = title
  }

  getTitle() {
    return this.title
  }

  setContent(content) {
    this.content = content
  }

  getContent() {
    return this.content
  }

  setHandleConfirm(func) {
    this.func.handleConfirm = func
  }

  getHandleConfirm() {
    return this.func.handleConfirm
  }
}

export default PopupModal