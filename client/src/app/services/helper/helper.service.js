import CryptoJS from 'crypto-js'

class HelperService {
  constructor() {

  }

  hashMD5(string) {
    return CryptoJS.MD5(string).toString()
  }

  hashSHA256(string) {
    return CryptoJS.SHA256(string).toString()
  }

  encryptAES(string, key) {
    const encrypted = CryptoJS.AES.encrypt(string, key)
    return encrypted.toString()
  }

  decryptAES(encrypted, key) {
    const decrypted = CryptoJS.AES.decrypt(encrypted, key)
    return decrypted.toString(CryptoJS.enc.Utf8)
  }

}

export default new HelperService()