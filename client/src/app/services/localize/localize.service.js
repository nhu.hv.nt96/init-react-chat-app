import HelperService from '../helper/helper.service'
import LoggerService from '../logger/logger.services'
import AppConf from '../../../config/app.conf.json'

class LocalizeService {
  constructor() {
    this.languageCurrent = 'vn'
    this.files = [
      'public/login'
    ]
    // this.data = {}
  }

  getCache() {
    LoggerService.info("LocalizeService excute getCache")
    try {
      const key = HelperService.hashMD5(AppConf.key)
      const language = window.localStorage.getItem(key)
      return language
    } catch (e) {
      LoggerService.error(`LocalizeService getCache ${e.toString()}`)
    }
  }

  getData() {
    let data = {}
    this.files.map((file) => {
      const localize  = require(`../../../assets/localize/${file}/${this.languageCurrent}.json`)
      data = Object.assign({}, data, localize)
    })
    return data
  }

  getLocalize(key) {
    if(this.getCache()) {
      this.languageCurrent = this.getCache()
    }
    return this.getData()[key]
  }

  changeLanguage(language) {
    this.languageCurrent = language
    const key = HelperService.hashMD5(AppConf.key)
    window.localStorage.setItem(key, language)
  }
}

export default new LocalizeService()