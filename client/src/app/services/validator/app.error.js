export default class AppError extends Error {

  constructor(field, message) {

    super(field, message)
    this.field = field;
    this.message = message;
  }
}