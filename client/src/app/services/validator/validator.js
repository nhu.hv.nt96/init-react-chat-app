import LoggerService from '../logger/logger.services'
import AppError from './app.error'
class ValidatorService {
  constructor() {
  }

  isNotEmpty(filed, value, messagge) {
    LoggerService.info("ValidatorService excute isNotEmpty")
    try {
      if (typeof value === 'undefined') {
        throw new AppError(filed, messagge)
      }
      if (value === '') {
        throw new AppError(filed, messagge)
      }
      if (value === null) {
        throw new AppError(filed, messagge)
      }
    } catch (e) {
      throw (e)
    }
  }

  isString(field, value, message) {
    LoggerService.info("ValidatorService excute isString")
    try {
      value = value.trim()
      if (typeof value !== 'string') {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isNumber(field, value, message) {
    LoggerService.info("ValidatorService excute isNumber")
    try {
      if (typeof value !== 'number') {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isArray(field, value, message) {
    LoggerService.info("ValidatorService excute isArray")
    try {
      if (!Array.isArray(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isObject(field, value, message) {
    LoggerService.info("ValidatorService excute isObject")
    try {
      this.isNotEmpty(field, value, message)
      if (typeof value !== "object") {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isUsername(field, value, message) {
    try {
      const regex = new RegExp(/^[a-zA-Z0-9@\.\-_]{6,45}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isPassword(field, value, message) {
    try {
      const regex = new RegExp(/^(?=^.{6,35}$)((?=.*[A-Z])(?=.*[a-z])(?=.*\d+)).*$/)
      const regex_ = new RegExp(/^[\w\d\@\!\%\&\*\(\)\_\-\=\+\"\/\\\?\>\<\:\;\.\,]+$/);
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
      if (!regex_.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isPhoneNumber(field, value, message) {
    try {
      const regex = new RegExp(/^[0-9]{10,13}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isPhoneNumberView(field, value, message) {
    try {
      const regex = new RegExp(/^[a-z0-9]{6,13}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isIdentityNo(field, value, message) {
    try {
      const regex = new RegExp(/^[a-zA-Z0-9]{8,12}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isFullName(field, value, message) {
    try {
      const regex = new RegExp(/^(?=.*[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ])(?=.*[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ])[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\d\s]{1,45}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isDate(field, value, message) {
    try {
      // value dd/MM/YYYY
      const regex = new RegExp(/(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isDateRegisterBooking(field, value, message) {
    try {
      // value dd/MM/YYYY
      const date = Helper.formatDate(value).getTime()
      const now = new Date().getTime()
      if (date < now) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isEmail(field, value, message) {
    try {
      const regex = new RegExp(/^([\w-.]+)@((\[[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.)|(([\w-]+.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isCreditAccount(field, value, message) {
    try {
      const regex = new RegExp(/^[0-9]{6,20}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isCreditCard(field, value, message) {
    try {
      const regex = new RegExp(/^[0-9]{1,20}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isCreditName(field, value, message) {
    try {
      const regex = new RegExp(/^[a-zA-Z0-9\s]{1,45}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isAmountTransfer(field, value, message) {
    try {
      const regex = new RegExp(/^[0-9]{5,13}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isAmountTarget(field, value, message) {
    try {
      const regex = new RegExp(/^[0-9]{6,13}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isAmountTerm(field, value, message) {
    try {
      const regex = new RegExp(/^[0-9]{7,12}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isAmountTermLeastCommonMultiple(field, value, message, leastCommonMultiple) {
    try {
      if (value % leastCommonMultiple !== 0) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }


  isAmountRecurring(field, value, message) {
    try {
      const regex = new RegExp(/^[0-9]{7,13}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isAmountWithdrawMin(field, value, message) {
    try {
      const regex = new RegExp(/^[0-9]{6,13}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  clearVietnamese(str) {
    try {
      str = str.toLowerCase();
      str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
      str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
      str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
      str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
      str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
      str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
      str = str.replace(/đ/g, "d");
      return str
    } catch (e) {
      return str
    }
  }

  isDesciptionTransfer(field, value, message) {
    try {
      const regex = new RegExp(/^[\w\d@!%&#$^*()`|~{}_\-=+"'\/\\?><:;.,\[\]\s]{0,200}$/);
      if (!regex.test(this.clearVietnamese(value))) throw new AppError(field, message)
    } catch (e) {
      throw e
    }
  }

  isBillCustomerId(field, value, message) {
    try {
      const regex = new RegExp(/^[a-zA-Z0-9\s]{1,16}$/)
      if (!regex.test(value)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isGoalName(field, value, message) {
    try {
      const clearVietnamese = this.clearVietnamese(value)
      const regex = new RegExp(/^[a-zA-Z0-9\s]{1,25}$/)
      if (!regex.test(clearVietnamese)) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  // isProperty(object, property) {
  //   try {
  //     this.isObject(object)
  //     this.isNotEmpty(property)
  //     if (!object.hasOwnProperty(property)) {
  //       throw new Error('Object is not property')
  //     }
  //   } catch (e) {
  //     throw new Error(e.message)
  //   }
  // }

  isCheckMinAmountWithdrawTerm(field, amount, min, message) {
    try {
      if (parseInt(amount) < min) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }

  isCheckMaxAmountWithdrawTerm(field, amount, max, message) {
    try {
      if (parseInt(amount) > max) {
        throw new AppError(field, message)
      }
    } catch (e) {
      throw e
    }
  }
}

export default new ValidatorService()