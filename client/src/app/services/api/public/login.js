import LoggerService from '../../logger/logger.services'
import BaseApi from '../base.api'

class LoginApi extends BaseApi {
  constructor() {

  }

  login() {
    LoggerService.info("LoginApi excute login")
    try {
      const option = {
        method: 'post',
        eaders: {
          'content-type': 'application/json'
        },
        data: '',
        url: ''
      }
      this.request(option)
    } catch (e) {
      LoggerService.error(`LoginApi login ${e.toString()}`)
    }
  }

}

export default new LoginApi()