
import LoogerService from '../logger/logger.services'
import Axios from 'axios'

class BaseApiService {
  constructor() {
    this.axios = Axios.create({
      baseURL: '',
      timeout: 1000
    });
  }

  request(option) {
    LoogerService.info("BaseApiService excute request")
    try {
      return this.axios.request(option)
    } catch(e) {
      LoogerService.error(`BaseApiService request`)
    }
  }

}

export default BaseApiService