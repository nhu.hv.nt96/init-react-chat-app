import MessageCode from '../message/message.code'
import LoginProcess from './public/login.process'
import LoggerService from '../../logger/logger.services'
class ProcessCodeResponse {
  constructor() {
    if(!ProcessCodeResponse.instance) {
      this.code = new Map([])

      this.code.set(MessageCode.LOGIN_SUCCESS,LoginProcess.login)
      ProcessCodeResponse.instance = this
    }
    return ProcessCodeResponse.instance
  }

  checkCode(...props) {
    LoggerService.info("ProcessCodeResponse excute checkCode")
    LoggerService.debug("receipt props", props)
    try {
      const [ responseCode ] = props
      LoggerService.debug("receive responseCode", responseCode)
      if(!this.code.has(responseCode)) {
        throw new Error(`Code ${responseCode} is not`)
      }
      this.code.get(responseCode)(props)
    } catch(e) {
      LoggerService.error(`ProcessCodeResponse checkCode ${e.toString()}`)
    }
  }

}

export default new ProcessCodeResponse()