import LoggerService from '../../../logger/logger.services'

class LoginProcess {
  constructor() {

  }

  login(props) {
    LoggerService.info("LoginProcess excute login")
    LoggerService.debug("receipt props", props)
    try {
      const [responseCode, state] = props
      state.func.handleLoginSuccess()
    } catch(e) {
      LoggerService.error(`LoginProcess login ${e.toString()}`)
    }
  }


}

export default new LoginProcess()