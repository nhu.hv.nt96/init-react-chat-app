import React, { component, Component } from 'react'

export default class InputComponent extends Component {
  constructor(props) {
    super(props)
  }

  render() {

    const {
      name,
      id,
      iconLeft = null,
      iconRight = null,
      error,
      type,
      placeholder,
      label,
      value,
      onChange
    } = this.props

    return (
      <div className="input-wrap">
        <label htmlFor={id}>{label}</label>
        <input
          id={id}
          type={type}
          placeholder={placeholder}
          name={name}
          value={value}
          onChange={onChange}
        />
        <div className="icon-left">
          <span>{iconLeft}</span>
        </div>
        <div className="icon-right">
          <span>{iconRight}</span>
        </div>
        <div className="error">
          <p>{error}</p>
        </div>

      </div>
    )
  }
}

