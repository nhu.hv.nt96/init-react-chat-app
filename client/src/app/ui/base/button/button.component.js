import React, { Component, Fragment } from 'react'

class ButtonComponent extends Component {
  constructor(props) {
    super(props)
  }


  render() {
    const {disabled, label, handleOnSubmit} = this.props
    return (
      <Fragment>
        <button 
        type="button" 
        className="btn btn-primary btn-block" 
        disabled={disabled}
        onClick={handleOnSubmit}
        >
          {label}
        </button>
      </Fragment>
    )
  }
}

export default ButtonComponent