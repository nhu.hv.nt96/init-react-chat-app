import React, { Component, Fragment } from 'react'
import CancelIcon from '@material-ui/icons/Cancel';
import Logo from '../../../../../assets/images/logo.png'
import Button from '../../button/button.component'

class NotiHtml extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { handleClose, handleConfirm, popup } = this.props
    return (
      <Fragment>
        <div className="popup-wrap">
          <div className="popup-container">
            <div className="popup-content">
              <div className="popup-header">
                <a className={"popup-btn-close"} onClick={handleClose}><CancelIcon /></a>
              </div>
              <div style={{ background: "red", height: "5px" }}></div>
              <div className="popup-body">
                <div className="popup-label">{popup.title}</div>
                <div className="popup-logo">
                  <img src={Logo} alt="" />
                </div>
                <div className="popup-text">{popup.content}</div>
              </div>
              <div className="popup-footer">
                <Button
                  label={"Đồng ý"}
                  handleOnSubmit={handleConfirm}
                />
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default NotiHtml