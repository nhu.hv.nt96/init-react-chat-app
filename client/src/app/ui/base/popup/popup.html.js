import React, { Component, Fragment } from 'react'
import CancelIcon from '@material-ui/icons/Cancel';
import Logo from '../../../../assets/images/logo.png'
import Button from '../button/button.component'

class PopupHtml extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { view, handleClose, handleConfirm, ConfirmContainer, NotiContainer } = this.props

    const showView = () => {
      switch (view.current) {
        case view.default:
          return null
        case view.noti:
          return <NotiContainer handleClose={handleClose} handleConfirm={handleConfirm} />
        case view.confirm:
          return <ConfirmContainer handleClose={handleClose} handleConfirm={handleConfirm} />
      }
    }
    return (
      <Fragment>
        {showView()}
      </Fragment>
    )
  }
}

export default PopupHtml