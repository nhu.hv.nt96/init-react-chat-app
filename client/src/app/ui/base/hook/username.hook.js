import React, { useEffect, useState, Fragment } from 'react'
import InputComponent from '../input/input.component'
import Validator from '../../../services/validator/validator'

const HookUsername = props => {
  const [username, setUsername] = useState(props.value)
  const [error, setError] = useState(props.error)

  useEffect(()=>{
    setError(props.error)
  },[props.error])

  const onChange = (event) => {
    try {
      setError('')
      setUsername(event.target.value)
      Validator.isNotEmpty('username', event.target.value, "Username khong duoc rong")
      Validator.isUsername('username', event.target.value, "Username khong dung")
      props.handleOnChange(event)
    } catch (e) {
      event.target.value = ''
      setError(e.message)
      props.handleOnChange(event)
    }

  }
  
  return (
    <InputComponent
      name={props.name}
      id={props.id}
      iconLeft={props.iconLeft}
      iconRight={props.iconRight}
      error={error}
      type={props.type}
      placeholder={props.placeholder}
      label={props.label}
      type={"text"}
      value={username}
      onChange={onChange}
    />
  )

}

export default HookUsername