import React, { useEffect, useState, Fragment } from 'react'
import InputComponent from '../input/input.component'
import Validator from '../../../services/validator/validator'

const HookPassword = props => {
  const [password, setPassword] = useState(props.value)
  const [error, setError] = useState(props.error)

  useEffect(()=>{
    setError(props.error)
  },[props.error])

  const onChange = (event) => {
    try {
      setError('')
      setPassword(event.target.value)
      Validator.isNotEmpty('password', event.target.value, "Password khong duoc rong")
      Validator.isPassword('password', event.target.value, "Password khong hop le")
      props.handleOnChange(event)
    } catch(e) {
      event.target.value = ''
      setError(e.message)
      props.handleOnChange(event)
    }
    
  }


  return (
    <InputComponent
      name={props.name}
      id={props.id}
      iconLeft={props.iconLeft}
      iconRight={props.iconRight}
      error={error}
      type={props.type}
      placeholder={props.placeholder}
      label={props.label}
      type={"password"}
      value={password}
      onChange={onChange}
    />
  )

}

export default HookPassword