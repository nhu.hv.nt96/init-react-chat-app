import React, { Component, Fragment } from 'react'
import Nhuhuynh from '../../../../assets/images/nhuhuynh.jpg'
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';
import UsernameHook from '../../base/hook/username.hook'
import PasswordHook from '../../base/hook/password.hook'
import ButtonComponent from '../../base/button/button.component'
import Localize from '../../../services/localize/localize.service'

export default class LoginHtml extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { ui, timeout, handleOnChange, handleOnSubmit, handleResult, handleTimeout, handlePopup, handleRedirect, handleChangeLanguage } = this.props
    return (
      <Fragment>
        <div className="wrap-login">
          <div className='login'>
            <div className="left-login">
              <img src={Nhuhuynh} />
            </div>
            <div className="right-login">
              <div className="right-border">
                <UsernameHook
                  label={Localize.getLocalize('PUBLIC_USERNAME')}
                  name={"username"}
                  value={''}
                  id={"username"}
                  placeholder={Localize.getLocalize('PUBLIC_USERNAME')}
                  iconLeft={<PersonIcon />}
                  error={timeout.field === 'username' ? timeout.message : null}
                  handleOnChange={handleOnChange}
                />
                <PasswordHook
                  label={Localize.getLocalize('PUBLIC_PASSWORD')}
                  name={"password"}
                  value={''}
                  id={"password"}
                  placeholder={Localize.getLocalize('PUBLIC_PASSWORD')}
                  iconLeft={<LockIcon />}
                  error={''}
                  handleOnChange={handleOnChange}
                />
                <ButtonComponent
                  handleOnSubmit={handleTimeout}
                  // handleOnSubmit={handlePopup}
                  disabled={ui.ishowBtn || timeout.status}
                  label={Localize.getLocalize('PUBLIC_LOGIN')}
                />

                <div className="alert-danger mt-1">
                  {timeout.field === 'all' ? timeout.message : null}
                </div>
                <div style={{ textAlign: "center", marginTop: "10px" }}>
                  <p>
                    <a href="#" onClick={(e) => {
                      e.preventDefault();
                      handleRedirect('/public/forgot')
                    }}>{Localize.getLocalize('PUBLIC_FORGOT_PASSWORD')}</a>
                    <a href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        handleRedirect('/public/register')
                      }}>
                      {Localize.getLocalize('PUBLIC_REGISTER')}
                    </a>
                  </p>
                </div>
                <div className="language">
                  <p><span className={Localize.languageCurrent === 'vn'? 'active' : ''} onClick={(event) => handleChangeLanguage('vn')}>VN</span>/<span className={Localize.languageCurrent === 'en' ? 'active' : ''} onClick={(event) => handleChangeLanguage('en')}>EN</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>

    )
  }
}